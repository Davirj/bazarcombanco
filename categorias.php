<?php
	session_start();
	if( !isset( $_SESSION["usuario"] ) )
	{
		Header("location: inicio.php");
    }
    
    $pdo = new PDO("mysql:host=localhost;dbname=bazarbanco;charset=utf8mb4","root","12345");
    

    $stment = $pdo->query('SELECT * FROM categorias');
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo</title>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">
        <?php if(isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
        }?>
        
		<table border="1">
            <thead>
                <tr>
                    <td>Id</td>
                    <td>Nome</td>
                </tr>
            </thead>

			<tbody>
                <?php foreach ($stment as $row) { ?>
                
                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['nome'] ?></td>
                </tr>

                <?php } ?>		
			</tbody>
		</table>

        <ul>
            <li><a href="adiciona_cat.php">Adicionar categoria</a></li>
            <li><a href="index.php">Voltar</a></li>        
        </ul>
	</div>

	<?php require_once("rodape.inc"); ?>

</body>
</html>