<?php
	session_start();
	if( !isset( $_SESSION["usuario"] ) || !isset($_POST['nome']) )
	{
		Header("location: inicio.php");
    }
    
    $pdo = new PDO("mysql:host=localhost;dbname=bazarbanco;charset=utf8mb4","root","12345");
    
    $stmt = $pdo->prepare("INSERT INTO `bazarbanco`.`categorias` (`nome`) VALUES (:nome)");
    
    $nome = $_POST['nome'];
    
    $stmt->bindParam(':nome', $nome, PDO::PARAM_STR);

    $stmt->execute();
    
    $_SESSION['msg'] = 'Categoria adicionada com sucesso';

    Header("location: categorias.php");
?>
